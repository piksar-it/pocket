<?php namespace Pocket\Transactions\Domain;

use Infrastructure\Persistence\Repository;
use Pocket\Shared\TransactionId;

interface TransactionRepository extends Repository {

    public function find(TransactionId $id): ?Transaction;

    public function save(Transaction $Transaction): void;

    public function delete(TransactionId $id): void;

}