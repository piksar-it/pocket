<?php 
namespace Pocket\Transactions\Domain\Event;

use Quasar\Ddd\Event\DomainEvent;

class RegisteredExchange extends DomainEvent
{
    protected function validateData(array $data = [])
    {
        return [
            'sourceAssetValueId' => true,
            'targetAssetValueId' => true,
            'quantity' => true,
            'price' => true
        ];
    }
}