<?php 
namespace Pocket\Transactions\Domain\Event;

use Quasar\Ddd\Event\DomainEvent;

class RegisteredDeposit extends DomainEvent
{
    protected function validateData(array $data = [])
    {
        return [
            'assetValueId' => true,
            'quantity' => true
        ];
    }
}