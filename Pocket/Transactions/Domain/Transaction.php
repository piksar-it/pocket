<?php namespace Pocket\Transactions\Domain;

use DateTime;
use Pocket\Shared\AssetValueId;
use Pocket\Shared\TransactionId;
use Pocket\Transactions\Application\Register\RegisterDepositCommand;
use Pocket\Transactions\Application\Register\RegisterWithdrawalCommand;
use Pocket\Transactions\Application\Register\RegisterExchangeCommand;
use Pocket\Transactions\Domain\Event\RegisteredDeposit;
use Pocket\Transactions\Domain\Event\RegisteredExchange;
use Pocket\Transactions\Domain\Event\RegisteredWithdrawal;
use Quasar\Ddd\AggregateRoot;
use Quasar\Ddd\IdentifiableByUuid;

class Transaction extends AggregateRoot {

    use IdentifiableByUuid;

    private DateTime $dateTime;
    private ?AssetValueId $sourceAssetValueId = null;
    private ?AssetValueId $targetAssetValueId = null;
    private float $quantity;
    private ?float $price = null;
    protected ?string $description = null;

    public static function deposit(RegisterDepositCommand $command): self
    {
        $transaction = new self();

        $transaction->uuid = new TransactionId();
        $transaction->dateTime = $command->dateTime ?? new DateTime('now');
        $transaction->targetAssetValueId = new AssetValueId($command->assetValueId);
        $transaction->quantity = $command->quantity;
        $transaction->description = $command->description;

        $transaction->record(new RegisteredDeposit($transaction->uuid, [
            'dateTime'           => $transaction->dateTime,
            'targetAssetValueId' => $command->assetValueId,
            'quantity'           => $command->quantity
        ]));

        return $transaction;
    }
    
    public static function withdrawal(RegisterWithdrawalCommand $command): self
    {
        $transaction = new self();

        $transaction->uuid = new TransactionId();
        $transaction->dateTime = $command->dateTime ?? new DateTime('now');
        $transaction->sourceAssetValueId = new AssetValueId($command->assetValueId);
        $transaction->quantity = $command->quantity;
        $transaction->description = $command->description;

        $transaction->record(new RegisteredWithdrawal($transaction->uuid, [
            'dateTime'           => $transaction->dateTime,
            'sourceAssetValueId' => $command->assetValueId,
            'quantity'           => $command->quantity
        ]));

        return $transaction;
    }

    public static function exchange(RegisterExchangeCommand $command): self
    {
        $transaction = new self();

        $transaction->uuid = new TransactionId();
        $transaction->dateTime = $command->dateTime ?? new DateTime('now');
        $transaction->sourceAssetValueId = new AssetValueId($command->sourceAssetValueId);
        $transaction->targetAssetValueId = new AssetValueId($command->targetAssetValueId);
        $transaction->quantity = $command->quantity;
        $transaction->price = $command->price;
        $transaction->description = $command->description;

        $transaction->record(new RegisteredExchange($transaction->uuid, [
            'dateTime'           => $transaction->dateTime,
            'sourceAssetValueId' => $command->sourceAssetValueId,
            'targetAssetValueId' => $command->targetAssetValueId,
            'quantity'           => $command->quantity,
            'price'              => $command->price
        ]));

        return $transaction;
    }

    public function restoreFromMemento(array $snapshot): void
    {
        $this->uuid = new TransactionId($snapshot['uuid']);
        $this->dateTime = new DateTime($snapshot['dateTime']);
        $this->sourceAssetValueId = $snapshot['sourceAssetValueId'] ? new AssetValueId($snapshot['sourceAssetValueId']) : null;
        $this->targetAssetValueId = $snapshot['targetAssetValueId'] ? new AssetValueId($snapshot['targetAssetValueId']) : null;
        $this->quantity = $snapshot['quantity'];
        $this->price = $snapshot['price'];
        $this->description = $snapshot['description'];
    }

    public function toMemento(): array
    {
        return [
            'uuid'               => (string) $this->uuid,
            'dateTime'           => $this->dateTime->format("Y-m-d H:i:s"),
            'sourceAssetValueId' => $this->sourceAssetValueId ? (string) $this->sourceAssetValueId : null,
            'targetAssetValueId' => $this->targetAssetValueId ? (string) $this->targetAssetValueId : null,
            'quantity'           => $this->quantity,
            'price'              => $this->price,
            'description'        => $this->description,
        ];
    }
}