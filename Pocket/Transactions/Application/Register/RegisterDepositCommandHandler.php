<?php namespace Pocket\Transactions\Application\Register;

use Pocket\Transactions\Domain\Transaction;
use Pocket\Transactions\Domain\TransactionRepository;
use Quasar\Cqrs\Command;
use Quasar\Cqrs\CommandHandler;
use Quasar\Ddd\Event\DomainEventPublisher;

class RegisterDepositCommandHandler implements CommandHandler
{
    private $transactionRepository;

    private $domainEventPublisher;

    public function __construct(TransactionRepository $repository, DomainEventPublisher $publisher)
    {
        $this->transactionRepository = $repository;    
        $this->domainEventPublisher = $publisher;    
    }

    /**
     * @param RegisterDepositCommand $command
     */
    public function handle(Command $command): void
    {
        $transaction = Transaction::deposit($command);

        $this->transactionRepository->save($transaction);

        $this->domainEventPublisher->publish(...$transaction->pullDomainEvents());
    }
}