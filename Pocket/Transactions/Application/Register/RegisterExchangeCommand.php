<?php namespace Pocket\Transactions\Application\Register;

use DateTime;
use Quasar\Cqrs\AbstractCommand;
use Quasar\Cqrs\Command;

class RegisterExchangeCommand extends AbstractCommand implements Command
{
    protected ?DateTime $dateTime;
    protected string $sourceAssetValueId;
    protected string $targetAssetValueId;
    protected float $quantity;
    protected float $price;
    protected ?string $description = null;

    public function __construct(
        string $sourceAssetValueId,
        string $targetAssetValueId,
        float $quantity,
        float $price,
        ?DateTime $dateTime = null,
        ?string $description = null
    )
    {
        $this->sourceAssetValueId = $sourceAssetValueId;
        $this->targetAssetValueId = $targetAssetValueId;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->dateTime = $dateTime;
        $this->description = $description;
    }

}