<?php namespace Pocket\Transactions\Application\Register;

use DateTime;
use Quasar\Cqrs\AbstractCommand;
use Quasar\Cqrs\Command;

class RegisterWithdrawalCommand extends AbstractCommand implements Command
{
    protected ?DateTime $dateTime;
    protected string $assetValueId;
    protected float $quantity;
    protected ?string $description = null;

    public function __construct(
        string $assetValueId,
        float $quantity,
        ?DateTime $dateTime = null,
        ?string $description = null
    )
    {
        $this->assetValueId = $assetValueId;
        $this->quantity = $quantity;
        $this->dateTime = $dateTime;
        $this->description = $description;
    }

}