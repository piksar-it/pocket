<?php namespace Pocket\Transactions\Infrastructure;

use Cake\Database\Expression\QueryExpression;
use Infrastructure\Persistence\SqlConnection;
use Pocket\Transactions\Domain\Transaction;
use Pocket\Transactions\Domain\TransactionRepository;
use Pocket\Shared\TransactionId;

class TransactionRepositoryMysql extends SqlConnection implements TransactionRepository {

    public function find(TransactionId $id): ?Transaction
    {
        $query = $this->connection()->newQuery();

        $query->from('transactions')
            ->select([
                'uuid'               => 'transactions.uuid',
                'dateTime'           => 'transactions.datetime',
                'sourceAssetValueId' => 'transactions.source_asset_value_id',
                'targetAssetValueId' => 'transactions.target_asset_value_id',
                'quantity'           => 'transactions.quantity',
                'price'              => 'transactions.price',
                'description'        => 'transactions.description'
            ])
            ->where(function (QueryExpression $exp) use ($id) {
                $exp->eq('transactions.uuid', $id);
                 
                return $exp;
            });

        $data = $query->execute()->fetch('assoc');

        if (!$data) {
            return null;
        }

        return Transaction::newFromMemento($data);
    }
    
    public function save(Transaction $Transaction): void
    {
        $existingTransaction = $this->find($Transaction->identity());
        
        $memento = $Transaction->toMemento();

        try {
            $data = [
                'datetime'              => $memento['dateTime'],
                'source_asset_value_id' => $memento['sourceAssetValueId'],
                'target_asset_value_id' => $memento['targetAssetValueId'],
                'price'                 => $memento['price'],
                'quantity'              => $memento['quantity'],
                'description'           => $memento['description'],
            ];
            
            if (null === $existingTransaction) {
                $this->connection()->insert('transactions', array_merge([
                    'uuid' => $memento['uuid'],
                ], $data));
            } else {
                $this->connection()->update('transactions', $data, ['uuid' => (string) $memento['uuid']]);
            }
        } catch (\Throwable $e) {
            throw $e; 
            # todo: custom exception
        }
    }

    public function delete(TransactionId $TransactionId): void
    {
        # TODO soft delete
        $this->connection()->delete('transactions',
            ['uuid' => (string) $TransactionId]
        );
    }
}