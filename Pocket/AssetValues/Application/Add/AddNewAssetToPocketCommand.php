<?php namespace Pocket\AssetValues\Application\Add;

use Quasar\Cqrs\AbstractCommand;
use Quasar\Cqrs\Command;

class AddNewAssetToPocketCommand extends AbstractCommand implements Command
{
    protected string $symbol;
    protected string $pocketId;
    protected ?string $description = null;

    public function __construct(
        string $symbol,
        string $pocketId,
        ?string $description = null
    )
    {
        $this->symbol = $symbol;
        $this->pocketId = $pocketId;
        $this->description = $description;
    }
}