<?php namespace Pocket\AssetValues\Application\Add;

use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Domain\AssetValueRepository;
use Quasar\Cqrs\Command;
use Quasar\Cqrs\CommandHandler;
use Quasar\Ddd\Event\DomainEventPublisher;

class AddNewAssetToPocketCommandHandler implements CommandHandler
{
    private $assetValueRepository;

    private $domainEventPublisher;

    public function __construct(AssetValueRepository $repository, DomainEventPublisher $publisher)
    {
        $this->assetValueRepository = $repository;    
        $this->domainEventPublisher = $publisher;    
    }

    /**
     * @param AddNewAssetToPocketCommand $command
     */
    public function handle(Command $command): void
    {
        $assetValue = AssetValue::new($command);

        $this->assetValueRepository->save($assetValue);

        $this->domainEventPublisher->publish(...$assetValue->pullDomainEvents());
    }
}