<?php namespace Pocket\AssetValues\Application\Update;

use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\Shared\AssetValueId;
use Pocket\Transactions\Domain\Event\RegisteredDeposit;
use Quasar\Ddd\Event\DomainEvent;
use Quasar\Ddd\Event\DomainEventPublisher;
use Quasar\Ddd\Event\DomainEventSubscriber;

class UpdateValueAfterDeposit implements DomainEventSubscriber {

    private $assetValueRepository;
    
    private $domainEventPublisher;

    public function __construct(AssetValueRepository $repository, DomainEventPublisher $publisher)
    {
        $this->assetValueRepository = $repository;
        $this->domainEventPublisher = $publisher;
    }

    public static function subscribedTo(): array
    {
        return [
            RegisteredDeposit::class,
        ];
    }

    /**
     * @param RegisteredDeposit $event
     */
    public function handle(DomainEvent $event): void
    {
        $targetAssetValue = $this->assetValueRepository->find(new AssetValueId($event->data()['assetValueId']));
        $targetAssetValue->updateBalance($event->data()['quantity']);

        $this->assetValueRepository->save($targetAssetValue);
        $this->domainEventPublisher->publish(...$targetAssetValue->pullDomainEvents());
    }
}