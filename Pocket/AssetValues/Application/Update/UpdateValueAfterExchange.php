<?php namespace Pocket\AssetValues\Application\Update;

use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\Shared\AssetValueId;
use Pocket\Transactions\Domain\Events\RegisteredExchange;
use Quasar\Ddd\Event\DomainEvent;
use Quasar\Ddd\Event\DomainEventPublisher;
use Quasar\Ddd\Event\DomainEventSubscriber;

class UpdateValueAfterExchange implements DomainEventSubscriber {

    private $assetValueRepository;
    
    private $domainEventPublisher;

    public function __construct(AssetValueRepository $repository, DomainEventPublisher $publisher)
    {
        $this->assetValueRepository = $repository;
        $this->domainEventPublisher = $publisher;
    }

    public static function subscribedTo(): array
    {
        return [
            RegisteredExchange::class,
        ];
    }

    /**
     * @param RegisteredExchange $event
     */
    public function handle(DomainEvent $event): void
    {
        $sourceAssetValue = $this->assetValueRepository->find(new AssetValueId($event->data()['sourceAssetValueId']));
        $cost = -$event->data()['quantity'] * $event->data()['price'];
        $sourceAssetValue->updateBalance(-$cost);

        $targetAssetValue = $this->assetValueRepository->find(new AssetValueId($event->data()['targetAssetValueId']));
        $targetAssetValue->updateBalance($event->data()['quantity']);

        $this->assetValueRepository->save($sourceAssetValue, $targetAssetValue);
        $this->domainEventPublisher->publish(...array_merge($sourceAssetValue->pullDomainEvents(), $targetAssetValue->pullDomainEvents()));
    }
}