<?php namespace Pocket\AssetValues\Application\Update;

use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\Shared\AssetValueId;
use Pocket\Transactions\Domain\Event\RegisteredWithdrawal;
use Quasar\Ddd\Event\DomainEvent;
use Quasar\Ddd\Event\DomainEventPublisher;
use Quasar\Ddd\Event\DomainEventSubscriber;

class UpdateValueAfterWithdrawal implements DomainEventSubscriber {

    private $assetValueRepository;
    
    private $domainEventPublisher;

    public function __construct(AssetValueRepository $repository, DomainEventPublisher $publisher)
    {
        $this->assetValueRepository = $repository;
        $this->domainEventPublisher = $publisher;
    }

    public static function subscribedTo(): array
    {
        return [
            RegisteredWithdrawal::class,
        ];
    }

    /**
     * @param RegisteredWithdrawal $event
     */
    public function handle(DomainEvent $event): void
    {
        $sourceAssetValue = $this->assetValueRepository->find(new AssetValueId($event->data()['assetValueId']));

        $sourceAssetValue->updateBalance(-$event->data()['quantity']);

        $this->assetValueRepository->save($sourceAssetValue);
        $this->domainEventPublisher->publish(...$sourceAssetValue->pullDomainEvents());
    }
}