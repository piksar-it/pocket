<?php namespace Pocket\AssetValues\Domain;

use Infrastructure\Persistence\Repository;
use Pocket\Shared\AssetValueId;

interface AssetValueRepository extends Repository {

    public function find(AssetValueId $id): ?AssetValue;

    public function save(AssetValue ...$assetValues): void;

    public function delete(AssetValueId ...$ids): void;

}