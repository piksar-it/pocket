<?php namespace Pocket\AssetValues\Domain;

use Pocket\AssetValues\Application\Add\AddNewAssetToPocketCommand;
use Pocket\AssetValues\Domain\Event\AddedAssetValueToPocket;
use Pocket\AssetValues\Domain\Event\UpdatedAssetValueBalance;
use Pocket\AssetValues\Domain\Exception\InsufficientBalanceException;
use Pocket\Shared\AssetSymbol;
use Pocket\Shared\AssetValueId;
use Pocket\Shared\PocketId;
use Quasar\Ddd\AggregateRoot;
use Quasar\Ddd\IdentifiableByUuid;

class AssetValue extends AggregateRoot {

    use IdentifiableByUuid;

    private PocketId $pocketId;
    private AssetSymbol $symbol;
    private float $quantity = 0;
    private string $description = '';

    public static function new(AddNewAssetToPocketCommand $command): self
    {
        $assetValue = new self();

        $assetValue->uuid = new AssetValueId();
        $assetValue->pocketId = new PocketId($command->pocketId);
        $assetValue->symbol = AssetSymbol::fromValue($command->symbol);
        $assetValue->description = $command->description ?? '';

        $assetValue->record(new AddedAssetValueToPocket($assetValue->uuid));
        
        return $assetValue;
    }

    public function updateBalance(float $value)
    {
        if ($this->quantity + $value < 0) {
            throw new InsufficientBalanceException('Insufficient balance');
        }

        $this->quantity+= $value;
        
        $this->record(new UpdatedAssetValueBalance($this->uuid, [
            'value' => $value
        ]));
    }

    public function restoreFromMemento(array $snapshot): void
    {
        $this->uuid = new AssetValueId($snapshot['uuid']);
        $this->pocketId = new PocketId($snapshot['pocketId']);
        $this->symbol = AssetSymbol::fromValue($snapshot['symbol']);
        $this->quantity = $snapshot['quantity'];
        $this->description = $snapshot['description'];
    }

    public function toMemento(): array
    {
        return [
            'uuid'        => (string) $this->uuid,
            'pocketId'    => (string) $this->pocketId,
            'symbol'      => (string) $this->symbol,
            'quantity'    => (float) $this->quantity,
            'description' => (string) $this->description,
        ];
    }
}
