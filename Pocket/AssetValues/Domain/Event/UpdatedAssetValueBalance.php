<?php namespace Pocket\AssetValues\Domain\Event;

use Quasar\Ddd\Event\DomainEvent;

class UpdatedAssetValueBalance extends DomainEvent
{
    protected function validateData(array $data = [])
    {
        return [
            'value' => true,
        ];
    }
    
}