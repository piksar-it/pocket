<?php namespace Pocket\AssetValues\Domain\Exception;

use Quasar\Ddd\DomainException;

class InsufficientBalanceException extends DomainException
{
}