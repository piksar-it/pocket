<?php namespace Pocket\AssetValues\Infrastructure;

use Cake\Database\Expression\QueryExpression;
use Infrastructure\Persistence\SqlConnection;
use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\Shared\AssetValueId;

class AssetValueRepositoryMysql extends SqlConnection implements AssetValueRepository {

    public function find(AssetValueId $id): ?AssetValue
    {
        $query = $this->connection()->newQuery();

        $query->from('asset_values')
            ->select([
                'uuid'        => 'asset_values.uuid',
                'pocketId'    => 'asset_values.pocket_id',
                'symbol'      => 'asset_values.symbol',
                'quantity'    => 'asset_values.quantity',
                'description' => 'asset_values.description'
            ])
            ->where(function (QueryExpression $exp) use ($id) {
                $exp->eq('asset_values.uuid', $id);
    
                return $exp;
            });

        $data = $query->execute()->fetch('assoc');

        if (!$data) {
            return null;
        }

        return AssetValue::newFromMemento($data);
    }
    
    public function save(AssetValue ...$assetValues): void
    {
        $this->connection()->begin();

        try {
            foreach ($assetValues as $assetValue) {
                $existingAssetValue = $this->find($assetValue->identity());
                
                $memento = $assetValue->toMemento();

                $data = [
                    'pocket_id'   => (string) $memento['pocketId'],
                    'symbol'      => (string) $memento['symbol'],
                    'quantity'    => (float) $memento['quantity'],
                    'description' => (string) $memento['description'],
                ];
                
                if (null === $existingAssetValue) {
                    $this->connection()->insert('asset_values', array_merge([
                        'uuid' => (string) $memento['uuid'],
                    ], $data));
                } else {
                    $this->connection()->update('asset_values', $data, ['uuid' => (string) $memento['uuid']]);
                }
            }

            $this->connection()->commit();

        } catch (\Throwable $e) {
            $this->connection()->rollback();

            # todo: custom exception
            throw $e; 
        }
    }

    public function delete(AssetValueId ...$assetValueIds): void
    {
        $this->connection()->begin();

        try {
            foreach ($assetValueIds as $assetValueId) {
                # TODO soft delete
                $this->connection()->delete('asset_values',
                    ['uuid' => (string) $assetValueId]
                );
            }

            $this->connection()->commit();

        } catch (\Throwable $e) {
            $this->connection()->rollback();

            # todo: custom exception
            throw $e; 
        }
    }
}