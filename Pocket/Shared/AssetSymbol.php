<?php
namespace Pocket\Shared;

use Quasar\Ddd\ValueObject\Enum;

class AssetSymbol extends Enum
{
    const BTC = 'btc';
    const USD = 'usd';
    const USDT = 'usdt';
    const ETH = 'eth';
    const ETC = 'etc';
    const PAXG = 'paxg';
}