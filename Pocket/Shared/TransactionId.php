<?php
namespace Pocket\Shared;

use Quasar\Ddd\IdentityInterface;
use Quasar\Ddd\ValueObject\Uuid;

class TransactionId extends Uuid implements IdentityInterface
{
    
}