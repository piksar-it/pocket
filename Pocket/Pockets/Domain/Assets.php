<?php namespace Pocket\Pockets\Domain;

use Quasar\Ddd\Collection;

class Assets extends Collection {

    protected $type = Asset::class;

    public function __construct(array $items = [])
    {
        $this->items = array_map(function (array $el) {
            return Asset::fromArray($el);
        }, $items);
    }
}
