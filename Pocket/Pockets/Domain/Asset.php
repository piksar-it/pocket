<?php namespace Pocket\Pockets\Domain;

use Pocket\Shared\AssetSymbol;
use Pocket\Shared\PocketId;
use Quasar\Ddd\Entity;
use Quasar\Ddd\IdentifiableByUuid;

use function Quasar\Ddd\uuid;

class Asset extends Entity {

    use IdentifiableByUuid;

    private AssetSymbol $symbol;
    private float $interest;
    private string $name = '';

    public static function create(AssetSymbol $symbol, float $interest, string $name): self
    {
        $asset = new self;

        $asset->uuid = new AssetId(uuid());
        $asset->symbol = $symbol;
        $asset->interest = $interest;
        $asset->name = $name;

        return $asset;
    }

    public function changeInterest(float $interest)
    {
        $this->interest = $interest;
    }

    public static function fromArray(array $snapshot): self
    {
        $asset = new self;

        $asset->uuid = new AssetId($snapshot['uuid']);
        $asset->name = $snapshot['name'];
        $asset->interest = $snapshot['interest'];
        $asset->symbol = AssetSymbol::fromValue($snapshot['symbol']);

        return $asset;
    }
    
    public function toArray(): array
    {
        return [
            'uuid'     => (string) $this->uuid,
            'name'     => (string) $this->name,
            'interest' => (float) $this->interest,
            'symbol'   => (string) $this->symbol,
        ];
    }
}
