<?php namespace Pocket\Pockets\Domain;

use Infrastructure\Persistence\Repository;
use Pocket\Shared\PocketId;

interface PocketRepository extends Repository {

    public function find(PocketId $id): ?Pocket;

    public function save(Pocket ...$pockets): void;

    public function delete(PocketId ...$ids): void;

}