<?php namespace Pocket\Pockets\Domain;

use Pocket\Pockets\Domain\Event\CreatedPocket;
use Pocket\Pockets\Application\Create\CreatePocketCommand;
use Pocket\Shared\AssetSymbol;
use Pocket\Shared\PocketId;
use Quasar\Ddd\AggregateRoot;
use Quasar\Ddd\DomainException;
use Quasar\Ddd\IdentifiableByUuid;
use Quasar\Ddd\ImmutableCollection;

class Pocket extends AggregateRoot {

    use IdentifiableByUuid;

    private string $name = '';
    private Assets $assets;

    public static function create(CreatePocketCommand $command): self
    {
        $pocket = new self();

        $pocket->uuid = new PocketId();
        $pocket->name = $command->name;
        $pocket->assets = new Assets([]);

        $pocket->record(new CreatedPocket($pocket->uuid));
        
        return $pocket;
    }

    public function addAsset(AssetSymbol $symbol, string $name)
    {
        $interest = $this->assets->count() == 0 ? 100 : 0;

        $asset = Asset::create($symbol, $interest, $name);

        $this->assets->add($asset);
    }

    public function rebalance(array $assetInterests)
    {
        # Temporary collection
        $assets = clone $this->assets;

        $changes = 0;

        foreach ($assetInterests as $uuid => $interest) {
            $asset = $assets->find(function(Asset $el) use ($uuid) {
                return $el->uuid == $uuid;
            })[0] ?? null;

            if (!$asset) {
                throw new DomainException('Asset not found');
            }

            $changes += ($asset->interest - $interest);

            $asset->changeInterest($interest);
        }

        if ($changes !== 0) {
            throw new DomainException('Sum of asset interests has to be 100');
        }

        # If everything is good flip collections
        $this->assets = $assets;
    }

    public function hasAsset(string $uuid) {
        return $this->assets->has(function (Asset $el) use ($uuid) {
            return $el->uuid == $uuid;
        });
    }

    public function assets()
    {
        return new class($this->assets->getItems()) extends ImmutableCollection {

        };
    }

    public function restoreFromMemento(array $snapshot): void
    {
        $this->uuid = new PocketId($snapshot['uuid']);
        $this->name = $snapshot['name'];
        $this->assets = new Assets($snapshot['assets']);
    }

    public function toMemento(): array
    {
        return [
            'uuid'   => (string) $this->uuid,
            'name'   => (string) $this->name,
            'assets' => $this->assets->toArray(),
        ];
    }
}
