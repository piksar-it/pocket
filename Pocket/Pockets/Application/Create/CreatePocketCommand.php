<?php namespace Pocket\Pockets\Application\Create;

use Quasar\Cqrs\AbstractCommand;
use Quasar\Cqrs\Command;

class CreatePocketCommand extends AbstractCommand implements Command
{
    protected string $name;
    protected array $assets;

    public function __construct(string $name, array $assets)
    {
        $this->name = $name;
        $this->setAssets($assets);
    }

    private function setAssets(array $assets) {

        # TODO: implement some validator

        $this->assets = $assets;
    }
}