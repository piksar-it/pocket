<?php namespace Pocket\Pockets\Application\Create;

use Pocket\Pockets\Domain\Pocket;
use Pocket\Pockets\Domain\PocketRepository;
use Quasar\Cqrs\Command;
use Quasar\Cqrs\CommandHandler;
use Quasar\Ddd\Event\DomainEventPublisher;

class CreatePocketCommandHandler implements CommandHandler
{
    private $pocketRepository;

    private $domainEventPublisher;

    public function __construct(PocketRepository $repository, DomainEventPublisher $publisher)
    {
        $this->pocketRepository = $repository;    
        $this->domainEventPublisher = $publisher;
    }

    /**
     * @param CreatePocketCommand $command
     */
    public function handle(Command $command): void
    {
        $pocket = Pocket::create($command);

        $this->pocketRepository->save($pocket);

        $this->domainEventPublisher->publish(...$pocket->pullDomainEvents());
    }
}