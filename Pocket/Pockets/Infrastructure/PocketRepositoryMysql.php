<?php namespace Pocket\Pockets\Infrastructure;

use Cake\Database\Expression\QueryExpression;
use Infrastructure\Persistence\SqlConnection;
use Pocket\Pockets\Domain\Pocket;
use Pocket\Pockets\Domain\PocketRepository;
use Pocket\Shared\PocketId;

class PocketRepositoryMysql extends SqlConnection implements PocketRepository {

    public function find(PocketId $id): ?Pocket
    {
        $query = $this->connection()->newQuery();

        $query->from('pockets')
            ->select([
                'uuid'           => 'pockets.uuid',
                'name'           => 'pockets.name',
                'asset_uuid'     => 'pocket_assets.uuid',
                'asset_name'     => 'pocket_assets.name',
                'asset_symbol'   => 'pocket_assets.symbol',
                'asset_interest' => 'pocket_assets.interest'
            ])
            ->join(['pocket_assets' => [
                    'table'      => 'pocket_assets',
                    'type'       => 'LEFT',
                    'conditions' => [
                        'pocket_assets.pocket_id = pockets.uuid',
                    ],
                ],
            ])->where(function (QueryExpression $exp) use ($id) {
                $exp->eq('pockets.uuid', $id);
                $exp->isNull('pockets.deleted_at');
    
                return $exp;
            });

        $rows = $query->execute()->fetchAll('assoc');

        if (!$rows) {
            return null;
        }

        $data = [
            'uuid' => $rows[0]['uuid'],
            'name' => $rows[0]['name'],
            'assets' => isset($rows[0]['asset_uuid']) ? array_map(function ($row) {
                return [
                    'uuid'     => $row['asset_uuid'],   
                    'name'     => $row['asset_name'],   
                    'symbol'   => $row['asset_symbol'],
                    'interest' => $row['asset_interest']
                ];
            }, $rows) : [],
        ];

        return Pocket::newFromMemento($data);
    }
    
    public function save(Pocket ...$pockets): void
    {
        $this->connection()->begin();

        try {
            foreach ($pockets as $pocket) {
                $existingPocket = $this->find($pocket->identity());
                
                $snapshot = $pocket->toMemento();

                $this->savePocket($snapshot, $existingPocket);
                $this->saveAssets($snapshot, $existingPocket);
            }

            $this->connection()->commit();

        } catch (\Throwable $e) {
            $this->connection()->rollback();

            # todo: custom exception
            throw $e; 
        }
    }

    private function savePocket(array $snapshot, ?Pocket $existingPocket)
    {
        if (null === $existingPocket) {
            $this->connection()->insert('pockets', [
                'uuid' => $snapshot['uuid'],
                'name' => $snapshot['name'],
            ]);

        } else {
            $this->connection()->update('pockets', [
                'name' => $snapshot['name']
            ], [
                'uuid' => $snapshot['uuid']
            ]);
        }
    }

    private function saveAssets(array $snapshot, ?Pocket $existingPocket)
    {
        foreach ($snapshot['assets'] as $asset) {
            if (null === $existingPocket || !$existingPocket->hasAsset($asset['uuid'])) {
                $this->connection()->insert('pocket_assets', [
                    'uuid'      => $asset['uuid'],
                    'pocket_id' => $snapshot['uuid'],
                    'name'      => $asset['name'],
                    'symbol'    => $asset['symbol'],
                    'interest'  => $asset['interest'],
                ]);
            } else {
                $this->connection()->update('pocket_assets', [
                    'name'     => $asset['name'],
                    'symbol'   => $asset['symbol'],
                    'interest' => $asset['interest'],
                ], [
                    'uuid' => $asset['uuid'],
                ]);
            }
        }
        
        $uuids = array_map(function ($asset) {return $asset['uuid']; }, $snapshot['assets']);

        $this->connection()->update('pocket_assets', [
            'deleted_at' => date('Y-m-d H:i:s')
        ], array_merge(
            [
                'deleted_at IS' => null,
                'pocket_id'     => $snapshot['uuid'],
            ],
            count($uuids) ? ['uuid NOT IN' => $uuids] : []
        ));
    }

    public function delete(PocketId ...$pocketIds): void
    {
        $this->connection()->begin();

        try {
            foreach ($pocketIds as $pocketId) {
                $this->connection()->update('pockets', [
                    'deleted_at' =>  date('Y-m-d H:i:s')
                ], [
                    'deleted_at IS' => null,
                    'uuid'          => (string) $pocketId,
                ]);
                
                $this->connection()->update('pocket_assets', [
                    'deleted_at' =>  date('Y-m-d H:i:s')
                ], [
                    'deleted_at IS' => null,
                    'pocket_id'     => (string) $pocketId,
                ]);
            }

            $this->connection()->commit();

        } catch (\Throwable $e) {
            $this->connection()->rollback();

            # todo: custom exception
            // var_dump($e);
            throw $e; 
        }
    }
}