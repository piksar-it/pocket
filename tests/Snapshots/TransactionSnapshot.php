<?php namespace Tests\Snapshots;

use function Quasar\Ddd\uuid;

class TransactionSnapshot
{
    public static function deposit(array $data = []): array
    {
        $transaction = self::exchange($data);

        $transaction['sourceAssetValueId'] = null;
        $transaction['price'] = null;
        $transaction['description'] = 'Deposit';

        return $transaction;
    }

    public static function withdrawal(array $data = []): array
    {
        $transaction = self::exchange($data);

        $transaction['targetAssetValueId'] = null;
        $transaction['price'] = null;
        $transaction['description'] = 'Withdrawal';

        return $transaction;
    }

    public static function exchange(array $data = []): array
    {
        return [
            'uuid'               => $data['uuid'] ?? uuid(),
            'sourceAssetValueId' => $data['sourceAssetValueId'] ?? uuid(),
            'targetAssetValueId' => $data['targetAssetValueId'] ?? uuid(),
            'quantity'           => $data['quantity'] ?? 0.02,
            'price'              => $data['price'] ?? 0.32,
            'description'        => $data['description'] ?? 'Exchange',
            'dateTime'           => $data['dateTime'] ?? '2020-10-10 10:10:10',
        ];
    }
}