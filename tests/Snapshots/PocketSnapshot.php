<?php namespace Tests\Snapshots;

use Pocket\Shared\AssetSymbol;

use function Quasar\Ddd\uuid;

class PocketSnapshot
{
    public static function random(array $data = []): array
    {
        $faker = \Faker\Factory::create();

        return [
            'uuid'     => $data['uuid'] ?? uuid(),
            'name'     => $data['name'] ?? $faker->name,
            'assets' => $data['assets'] ?? [
                [
                    'uuid'     => uuid(),
                    'name'     => $data['name'] ?? $faker->name,
                    'symbol'   => $data['symbol'] ?? $faker->randomElement(AssetSymbol::values()),
                    'interest' => $x1 = rand(1, 98),
                ],
                [
                    'uuid'     => uuid(),
                    'name'     => $data['name'] ?? $faker->name,
                    'symbol'   => $data['symbol'] ?? $faker->randomElement(AssetSymbol::values()),
                    'interest' => $x2 = rand(1, 99-$x1),
                ],
                [
                    'uuid'     => uuid(),
                    'name'     => $data['name'] ?? $faker->name,
                    'symbol'   => $data['symbol'] ?? $faker->randomElement(AssetSymbol::values()),
                    'interest' => 100 - $x1 - $x2,
                ],
            ],
        ];
    }
}