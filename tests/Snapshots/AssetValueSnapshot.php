<?php namespace Tests\Snapshots;

use function Quasar\Ddd\uuid;

class AssetValueSnapshot
{
    public static function random(array $data = []): array
    {
        return [
            'uuid'        => $data['uuid'] ?? uuid(),
            'pocketId'    => $data['pocketId'] ?? uuid(),
            'symbol'      => $data['symbol'] ?? 'etc',
            'quantity'    => $data['quantity'] ?? 0.02,
            'description' => $data['description'] ?? 'ETC hodl',
        ];
    }
}