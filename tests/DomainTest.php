<?php namespace Tests;

use Quasar\Ddd\AggregateRoot;
use Quasar\Ddd\ValueObject\Uuid;

abstract class DomainTest extends TestCase {

    public function randomUuidString()
    {
        // return (new UuidGenerator())->next();
    }

    public function randomUuid()
    {
        // return new Uuid($this->randomUuidString());
    }

    public function shouldBeDomainEventsCreated(AggregateRoot $aggregate, array $eventClasses)
    {
        $domainEvents = $aggregate->pullDomainEvents();

        foreach ($eventClasses as $eventClass) {
            $this->assertTrue(in_array($eventClass, array_map(function ($event) {
                return get_class($event);
            }, $domainEvents)));
        }
    }

}