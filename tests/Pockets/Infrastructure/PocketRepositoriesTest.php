<?php namespace Tests\Pockets\Infrastructure;

use Pocket\Pockets\Domain\Pocket;
use Pocket\Pockets\Infrastructure\PocketRepositoryMysql;
use Tests\RepositoryTest;
use Tests\Snapshots\PocketSnapshot;

class PocketRepositoriesTest extends RepositoryTest {

    public function test_save_find_and_delete()
    {
        foreach ($this->repositories() as $repository) {
            $exampleSnapshot = PocketSnapshot::random();
            
            $this->abstract_save_find_and_delete_test($repository, Pocket::class, $exampleSnapshot);
        }
    }
 
    private function repositories(): array
    {
        return [
            new PocketRepositoryMysql(container()->get('databaseConnection')),
        ];
    }
}