<?php namespace Tests\Pockets\Domain;

use Pocket\Pockets\Domain\Asset;
use Pocket\Pockets\Domain\Pocket;
use Pocket\Shared\AssetSymbol;
use Tests\DomainTest;
use Tests\Snapshots\PocketSnapshot;

class AddAssetTest extends DomainTest {

    public function test_first_asset_in_pocket()
    {
        $snapshot = PocketSnapshot::random(['assets' => []]);

        /** @var Pocket */
        $pocket = Pocket::newFromMemento($snapshot);

        $symbol = $this->faker->randomElement(AssetSymbol::values());
        $name = $this->faker->name;

        $pocket->addAsset(AssetSymbol::fromValue($symbol), $name);

        $asset = $pocket->assets()->getItems()[0];
        
        $this->assertInstanceOf(Asset::class, $asset);

        $this->assertEquals($symbol, $asset->symbol);
        $this->assertEquals($name, $asset->name);
        $this->assertEquals(100, $asset->interest);
        $this->assertNotNull($asset->uuid);
    }

    public function test_next_asset_in_pocket()
    {
        $snapshot = PocketSnapshot::random();

        /** @var Pocket */
        $pocket = Pocket::newFromMemento($snapshot);

        $symbol = $this->faker->randomElement(AssetSymbol::values());
        $name = $this->faker->name;

        $pocket->addAsset(AssetSymbol::fromValue($symbol), $name);

        $asset = array_values($pocket->assets()->find(function ($el) use ($symbol, $name) {
            return $el->symbol == $symbol && $el->name == $name;
        }))[0];
        
        $this->assertInstanceOf(Asset::class, $asset);

        $this->assertEquals($symbol, $asset->symbol);
        $this->assertEquals($name, $asset->name);
        $this->assertEquals(0, $asset->interest);
        $this->assertNotNull($asset->uuid);
    }
}