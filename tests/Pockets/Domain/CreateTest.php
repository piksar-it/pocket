<?php namespace Tests\Pockets\Domain;

use Pocket\Pockets\Application\Create\CreatePocketCommand;
use Pocket\Pockets\Domain\Pocket;
use Pocket\Pockets\Domain\Event\CreatedPocket;
use Tests\DomainTest;
use Tests\Snapshots\PocketSnapshot;

class CreateTest extends DomainTest {

    public function test_create()
    {
        $snapshot = PocketSnapshot::random();

        $command = new CreatePocketCommand(
            $snapshot['name'],
            $snapshot['assets']
        );

        $pocket = Pocket::create($command);

        $this->assertInstanceOf(Pocket::class, $pocket);

        $this->shouldBeDomainEventsCreated($pocket, [CreatedPocket::class]);
    }

}