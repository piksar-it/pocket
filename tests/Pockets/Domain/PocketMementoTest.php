<?php namespace Tests\Pockets\Domain;

use Pocket\Pockets\Domain\Pocket;
use Tests\DomainTest;
use Tests\Snapshots\PocketSnapshot;

class PocketMementoTest extends DomainTest {

    public function test_memento()
    {
        $snapshot = PocketSnapshot::random();

        $pocket = Pocket::newFromMemento($snapshot);

        $this->assertEquals($snapshot, $pocket->toMemento());
    }
}