<?php namespace Tests\Pockets\Domain;

use Pocket\Pockets\Domain\Pocket;
use Tests\DomainTest;
use Tests\Snapshots\PocketSnapshot;

use function Quasar\Ddd\uuid;

class HasAssetTest extends DomainTest {

    public function test_positive()
    {
        $snapshot = PocketSnapshot::random();

        /** @var Pocket */
        $pocket = Pocket::newFromMemento($snapshot);
        
        $this->assertTrue($pocket->hasAsset($snapshot['assets'][0]['uuid']));
    }

    public function test_negative()
    {
        $snapshot = PocketSnapshot::random();

        /** @var Pocket */
        $pocket = Pocket::newFromMemento($snapshot);
        
        $this->assertFalse($pocket->hasAsset(uuid()));
    }

}