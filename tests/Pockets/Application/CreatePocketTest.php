<?php namespace Tests\Pockets\Application;

use Pocket\Pockets\Domain\Event\CreatedPocket;
use Pocket\Pockets\Application\Create\CreatePocketCommand;
use Pocket\Pockets\Domain\PocketRepository;
use Tests\ApplicationTest;
use Tests\Snapshots\PocketSnapshot;

use function Quasar\Cqrs\dispatch;

class CreatePocketTest extends ApplicationTest {

    public function test_update_persistence_and_publisher()
    {
        $snapshot = PocketSnapshot::random();

        $pocketRepository = $this->mock(PocketRepository::class);
        $pocketRepository->shouldReceive('save')->once();
        
        container()->set(PocketRepository::class, $pocketRepository);
        
        $this->shouldPublishDomainEvents(CreatedPocket::class);

        dispatch(new CreatePocketCommand(
            $snapshot['name'],
            $snapshot['assets']
        ));
    }
}