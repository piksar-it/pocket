<?php namespace Tests\AssetValues\Domain;

use DateTime;
use Pocket\AssetValues\Application\Add\AddNewAssetToPocketCommand;
use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Domain\Event\AddedAssetValueToPocket;
use Tests\DomainTest;
use Tests\Snapshots\AssetValueSnapshot;

class AddTest extends DomainTest {

    public function test_new()
    {
        $snapshot = AssetValueSnapshot::random();

        $command = new AddNewAssetToPocketCommand(
            $snapshot['symbol'],
            $snapshot['pocketId']
        );

        // /** @var AssetValue */
        $assetValue = AssetValue::new($command);

        $this->assertInstanceOf(AssetValue::class, $assetValue);

        $this->shouldBeDomainEventsCreated($assetValue, [AddedAssetValueToPocket::class]);
    }

}