<?php namespace Tests\AssetValues\Domain;

use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Domain\Event\UpdatedAssetValueBalance;
use Pocket\AssetValues\Domain\Exception\InsufficientBalanceException;
use Tests\DomainTest;
use Tests\Snapshots\AssetValueSnapshot;

class UpdateBalanceTest extends DomainTest {

    /**
     * @dataProvider values
     */
    public function test_positive($value)
    {
        $snapshot = AssetValueSnapshot::random([
            'quantity' => 10
        ]);

        /** @var AssetValue */
        $assetValue = AssetValue::newFromMemento($snapshot);

        $assetValue->updateBalance($value);

        $assetValueData = $assetValue->toMemento();
        
        $this->assertEquals($snapshot['quantity'] + $value, $assetValueData['quantity']);

        $this->shouldBeDomainEventsCreated($assetValue, [UpdatedAssetValueBalance::class]);
    }

    public function test_insufficient_balance()
    {
        $this->expectException(InsufficientBalanceException::class);

        $snapshot = AssetValueSnapshot::random([
            'quantity' => 10
        ]);

        /** @var AssetValue */
        $assetValue = AssetValue::newFromMemento($snapshot);

        $value = -11;

        $assetValue->updateBalance($value);
    }

    
    public function values(): array
    {
        return [
            [-10],
            [-5],
            [0],
            [5.5],
            [10000],
        ];
    }

}