<?php namespace Tests\AssetValues\Domain;

use Pocket\AssetValues\Domain\AssetValue;
use Tests\DomainTest;
use Tests\Snapshots\AssetValueSnapshot;

class AssetValueMementoTest extends DomainTest {

    public function test_memento()
    {
        $snapshot = AssetValueSnapshot::random();

        $assetValue = AssetValue::newFromMemento($snapshot);

        $this->assertEquals($snapshot, $assetValue->toMemento());
    }
}