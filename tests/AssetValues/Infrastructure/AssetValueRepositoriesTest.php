<?php namespace Tests\AssetValues\Infrastructure;

use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Infrastructure\AssetValueRepositoryMysql;
use Tests\RepositoryTest;
use Tests\Snapshots\AssetValueSnapshot;

class AssetValueRepositoriesTest extends RepositoryTest {

    /**
     * @dataProvider exampleSnapshots
     */
    public function test_save_find_and_delete($exampleSnapshot)
    {
        foreach ($this->repositories() as $repository) {
            $this->abstract_save_find_and_delete_test($repository, AssetValue::class, $exampleSnapshot);
        }
    }
    
    public function test_update_quantity()
    {
        foreach ($this->repositories() as $repository) {
            $exampleSnapshot = AssetValueSnapshot::random();

            try {
                $repository->startTransaction();

                $exampleAggregate = AssetValue::newFromMemento($exampleSnapshot);

                $repository->save($exampleAggregate);

                $exampleSnapshot['quantity'] = $exampleSnapshot['quantity'] + 1;
                $exampleAggregate->restoreFromMemento($exampleSnapshot);

                $repository->save($exampleAggregate);
                
                $this->assertEqualsCanonicalizing($exampleSnapshot, $repository->find($exampleAggregate->identity())->toMemento());

            } finally {
                $repository->rollbackTransaction();
            }
        }
    }
    
    public function exampleSnapshots(): array
    {
        return [
            [AssetValueSnapshot::random()],
        ];
    }

    private function repositories(): array
    {
        return [
            new AssetValueRepositoryMysql(container()->get('databaseConnection')),
        ];
    }
}