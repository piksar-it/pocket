<?php namespace Tests\AssetValues\Application;

use Mockery;
use Pocket\AssetValues\Application\Update\UpdateValueAfterExchange;
use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\Shared\AssetValueId;
use Pocket\Shared\TransactionId;
use Pocket\Transactions\Domain\Event\RegisteredExchange;
use Tests\ApplicationTest;
use Tests\Snapshots\AssetValueSnapshot;
use Tests\Snapshots\TransactionSnapshot;

class UpdateValueAfterExchangeTest extends ApplicationTest {

    public function test_update_persistence_and_publisher()
    {
        $sourceAssetValueSnapshot = AssetValueSnapshot::random();
        $targetAssetValueSnapshot = AssetValueSnapshot::random();
        
        $transactionSnapshot = TransactionSnapshot::exchange([
            'sourceAssetValueId' => $sourceAssetValueSnapshot['uuid'],
            'targetAssetValueId' => $targetAssetValueSnapshot['uuid']
        ]);

        $sourceAssetValue = Mockery::spy(AssetValue::class);
        $targetAssetValue = Mockery::spy(AssetValue::class);

        $sourceAssetValue->restoreFromMemento($sourceAssetValueSnapshot);
        $targetAssetValue->restoreFromMemento($targetAssetValueSnapshot);
        
        $assetValueRepository = $this->mock(AssetValueRepository::class);
        container()->set(AssetValueRepository::class, $assetValueRepository);

        $assetValueRepository->shouldReceive('find')
            ->andReturnUsing(function (AssetValueId $uuid) use ($sourceAssetValueSnapshot, $sourceAssetValue, $targetAssetValue) {
                return (string) $uuid == $sourceAssetValueSnapshot['uuid'] ? $sourceAssetValue : $targetAssetValue;
            })->twice();
        
        $assetValueRepository->shouldReceive('save')
            ->withArgs([$sourceAssetValue, $targetAssetValue])
            ->once();

        $this->shouldPublishDomainEvents();
        
        $updateValueAfterExchange = container()->get(UpdateValueAfterExchange::class);

        $transactionId = new TransactionId($transactionSnapshot['uuid']);

        $updateValueAfterExchange->handle(new RegisteredExchange($transactionId, [
            'sourceAssetValueId' => $transactionSnapshot['sourceAssetValueId'],
            'targetAssetValueId' => $transactionSnapshot['targetAssetValueId'],
            'quantity'           => $transactionSnapshot['quantity'],
            'price'              => $transactionSnapshot['price'],
            'dateTime'           => $transactionSnapshot['dateTime']
        ]));

        $sourceAssetValue->shouldHaveReceived('updateBalance')->once();
        $targetAssetValue->shouldHaveReceived('updateBalance')->once();
    }
}