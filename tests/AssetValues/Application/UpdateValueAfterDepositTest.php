<?php namespace Tests\AssetValues\Application;

use Mockery;
use Pocket\AssetValues\Application\Update\UpdateValueAfterDeposit;
use Pocket\AssetValues\Domain\AssetValue;
use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\Shared\TransactionId;
use Pocket\Transactions\Domain\Event\RegisteredDeposit;
use Tests\ApplicationTest;
use Tests\Snapshots\AssetValueSnapshot;
use Tests\Snapshots\TransactionSnapshot;

class UpdateValueAfterDepositTest extends ApplicationTest {

    public function test_update_persistence_and_publisher()
    {
        $assetValueSnapshot = AssetValueSnapshot::random();
        $transactionSnapshot = TransactionSnapshot::deposit([
            'sourceAssetValueId' => $assetValueSnapshot['uuid']
        ]);

        $assetValue = Mockery::spy(AssetValue::class);

        $assetValue->restoreFromMemento($assetValueSnapshot);

        $assetValueRepository = $this->mock(AssetValueRepository::class);
        container()->set(AssetValueRepository::class, $assetValueRepository);

        $assetValueRepository->shouldReceive('find')->andReturn($assetValue);
        $assetValueRepository->shouldReceive('save')->withArgs([$assetValue])->once();

        $this->shouldPublishDomainEvents();
        
        $updateValueAfterDeposit = container()->get(UpdateValueAfterDeposit::class);

        $transactionId = new TransactionId($transactionSnapshot['uuid']);

        $updateValueAfterDeposit->handle(new RegisteredDeposit($transactionId, [
            'assetValueId' => $transactionSnapshot['sourceAssetValueId'],
            'quantity'     => $transactionSnapshot['quantity'],
            'price'        => $transactionSnapshot['price'],
            'dateTime'     => $transactionSnapshot['dateTime']
        ]));

        $assetValue->shouldHaveReceived('updateBalance')->once();
    }
}