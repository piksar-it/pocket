<?php namespace Tests\AssetValues\Application;

use Mockery;
use Pocket\AssetValues\Application\Add\AddNewAssetToPocketCommand;
use Pocket\AssetValues\Domain\AssetValueRepository;
use Pocket\AssetValues\Domain\Event\AddedAssetValueToPocket;
use Tests\ApplicationTest;
use Tests\Snapshots\AssetValueSnapshot;

use function Quasar\Cqrs\dispatch;

class AddNewAssetToPocketTest extends ApplicationTest {

    public function test_update_persistence_and_publisher()
    {
        $snapshot = AssetValueSnapshot::random();

        $assetValueRepository = $this->mock(AssetValueRepository::class);
        $assetValueRepository->shouldReceive('save')->once();
        
        container()->set(assetValueRepository::class, $assetValueRepository);
        
        $this->shouldPublishDomainEvents(AddedAssetValueToPocket::class);

        dispatch(new AddNewAssetToPocketCommand(
            $snapshot['symbol'],
            $snapshot['pocketId']
        ));
    }
}