<?php namespace Tests;

use Mockery;
use Quasar\Ddd\Event\DomainEventPublisher;

abstract class ApplicationTest extends DomainTest {

    protected $domainEventPublisher;

    protected function setUp(): void
    {
        require __DIR__ . '/../bootstrap.php';

        $this->domainEventPublisher = $this->mock(DomainEventPublisher::class);
        container()->set(DomainEventPublisher::class, $this->domainEventPublisher);

        // container()->set(ErrorReporterInterface::class, new NullErrorReporter());

        parent::setUp();
    }

    protected function domainEventPublisher()
    {
        return $this->domainEventPublisher = $this->domainEventPublisher ?: $this->mock(DomainEventPublisher::class);
    }

    protected function shouldPublishDomainEvents(...$events)
    {
        $publish = $this->domainEventPublisher()
            ->shouldReceive('publish')
            ->once();
        
        if (count($events) > 0) {
            $publish->withArgs(array_map(function ($event) use ($events) {
                    return Mockery::on(function ($argument) use ($events) {
                        return in_array(get_class($argument), $events);
                    });
                }, $events)
            );
        }
    }

}