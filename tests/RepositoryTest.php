<?php namespace Tests;

abstract class RepositoryTest extends DomainTest {

    protected function setUp(): void
    {
        require __DIR__ . '/../bootstrap.php';

        parent::setUp();
    }

    protected function abstract_save_find_and_delete_test(object $repository, string $aggregateClass, array $exampleSnapshot)
    {
        try {
            $repository->startTransaction();

            $exampleAggregate = $aggregateClass::newFromMemento($exampleSnapshot);

            $repository->save($exampleAggregate);

            $newAggregate = $repository->find($exampleAggregate->identity());

            $this->assertEquals(0, count(
                array_diff($exampleSnapshot, $newAggregate->toMemento()) + array_diff($newAggregate->toMemento(), $exampleSnapshot)
            ));

            $repository->delete($newAggregate->identity());

            $this->assertNull($repository->find($newAggregate->identity()));

        } finally {
            $repository->rollbackTransaction();
        }
    }
}