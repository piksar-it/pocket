<?php namespace Tests;

use Mockery;
use Mockery\MockInterface;

class TestCase extends \PHPUnit\Framework\TestCase {

    protected $faker;

    protected function setUp(): void
    {
        $this->faker = \Faker\Factory::create();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        $this->addToAssertionCount(Mockery::getContainer()->mockery_getExpectationCount());
        Mockery::close();

        parent::tearDown();
    }

    protected function mock($className) : MockInterface
    {
        return Mockery::mock($className);
    }

}