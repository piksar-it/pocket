<?php namespace Tests\Transactions\Domain;

use Pocket\Transactions\Domain\Transaction;
use Tests\DomainTest;
use Tests\Snapshots\TransactionSnapshot;

class TransactionMementoTest extends DomainTest {

    /**
     * @dataProvider exampleSnapshots
     */
    public function test_memento($snapshot)
    {
        $transaction = Transaction::newFromMemento($snapshot);

        $this->assertEquals($snapshot, $transaction->toMemento());
    }

    public function exampleSnapshots(): array
    {
        return [
            [TransactionSnapshot::deposit()],
            [TransactionSnapshot::withdrawal()],
            [TransactionSnapshot::exchange()],
        ];
    }
}