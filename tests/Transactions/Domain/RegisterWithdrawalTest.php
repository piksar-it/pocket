<?php namespace Tests\Transactions\Domain;

use DateTime;
use Pocket\Transactions\Application\Register\RegisterWithdrawalCommand;
use Pocket\Transactions\Domain\Event\RegisteredWithdrawal;
use Pocket\Transactions\Domain\Transaction;
use Tests\DomainTest;
use Tests\Snapshots\TransactionSnapshot;

class RegisterWithdrawalTest extends DomainTest {

    public function test_withdrawal()
    {
        $snapshot = TransactionSnapshot::withdrawal();

        $command = new RegisterWithdrawalCommand(
            $snapshot['sourceAssetValueId'],
            $snapshot['quantity'],
            new DateTime($snapshot['dateTime']),
        );

        $transaction = Transaction::withdrawal($command);

        $this->assertInstanceOf(Transaction::class, $transaction);

        $transactionData = $transaction->toMemento();
        
        $this->assertEquals($snapshot['sourceAssetValueId'], $transactionData['sourceAssetValueId']);
        $this->assertEquals($snapshot['quantity'], $transactionData['quantity']);
        $this->assertEquals($snapshot['dateTime'], $transactionData['dateTime']);

        $this->shouldBeDomainEventsCreated($transaction, [RegisteredWithdrawal::class]);
    }

}