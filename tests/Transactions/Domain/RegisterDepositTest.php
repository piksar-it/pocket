<?php namespace Tests\Transactions\Domain;

use DateTime;
use Pocket\Transactions\Application\Register\RegisterDepositCommand;
use Pocket\Transactions\Domain\Event\RegisteredDeposit;
use Pocket\Transactions\Domain\Transaction;
use Tests\DomainTest;
use Tests\Snapshots\TransactionSnapshot;

class RegisterDepositTest extends DomainTest {

    public function test_deposit()
    {
        $snapshot = TransactionSnapshot::deposit();

        $command = new RegisterDepositCommand(
            $snapshot['targetAssetValueId'],
            $snapshot['quantity'],
            new DateTime($snapshot['dateTime']),
        );

        $transaction = Transaction::deposit($command);

        $this->assertInstanceOf(Transaction::class, $transaction);

        $transactionData = $transaction->toMemento();
        
        $this->assertEquals($snapshot['targetAssetValueId'], $transactionData['targetAssetValueId']);
        $this->assertEquals($snapshot['quantity'], $transactionData['quantity']);
        $this->assertEquals($snapshot['dateTime'], $transactionData['dateTime']);

        $this->shouldBeDomainEventsCreated($transaction, [RegisteredDeposit::class]);
    }

}