<?php namespace Tests\Transactions\Domain;

use DateTime;
use Pocket\Transactions\Application\Register\RegisterExchangeCommand;
use Pocket\Transactions\Domain\Event\RegisteredExchange;
use Pocket\Transactions\Domain\Transaction;
use Tests\DomainTest;
use Tests\Snapshots\TransactionSnapshot;

class RegisterExchangeTest extends DomainTest {

    public function test_exchange()
    {
        $snapshot = TransactionSnapshot::exchange();

        $command = new RegisterExchangeCommand(
            $snapshot['sourceAssetValueId'],
            $snapshot['targetAssetValueId'],
            $snapshot['quantity'],
            $snapshot['price'],
            new DateTime($snapshot['dateTime']),
        );

        $transaction = Transaction::Exchange($command);

        $this->assertInstanceOf(Transaction::class, $transaction);

        $transactionData = $transaction->toMemento();
        
        $this->assertEquals($snapshot['sourceAssetValueId'], $transactionData['sourceAssetValueId']);
        $this->assertEquals($snapshot['targetAssetValueId'], $transactionData['targetAssetValueId']);
        $this->assertEquals($snapshot['quantity'], $transactionData['quantity']);
        $this->assertEquals($snapshot['price'], $transactionData['price']);
        $this->assertEquals($snapshot['dateTime'], $transactionData['dateTime']);

        $this->shouldBeDomainEventsCreated($transaction, [RegisteredExchange::class]);
    }

}