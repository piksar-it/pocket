<?php namespace Tests\Transactions\Application;

use DateTime;
use Pocket\Transactions\Application\Register\RegisterWithdrawalCommand;
use Pocket\Transactions\Domain\Event\RegisteredWithdrawal;
use Tests\ApplicationTest;
use Tests\Snapshots\TransactionSnapshot;
use Pocket\Transactions\Domain\TransactionRepository;

use function Quasar\Cqrs\dispatch;

class RegisterWithdrawalTest extends ApplicationTest {

    public function test_withdrawal()
    {
        $snapshot = TransactionSnapshot::withdrawal();

        $transactionRepository = $this->mock(TransactionRepository::class);
        $transactionRepository->shouldReceive('save')->once();
        
        container()->set(TransactionRepository::class, $transactionRepository);
        
        $this->shouldPublishDomainEvents(RegisteredWithdrawal::class);

        dispatch(new RegisterWithdrawalCommand(
            $snapshot['sourceAssetValueId'],
            $snapshot['quantity'],
            new DateTime($snapshot['dateTime']),
        ));
    }
}