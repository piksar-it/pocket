<?php namespace Tests\Transactions\Application;

use DateTime;
use Pocket\Transactions\Application\Register\RegisterDepositCommand;
use Pocket\Transactions\Domain\Event\RegisteredDeposit;
use Tests\ApplicationTest;
use Tests\Snapshots\TransactionSnapshot;
use Pocket\Transactions\Domain\TransactionRepository;

use function Quasar\Cqrs\dispatch;

class RegisterDepositTest extends ApplicationTest {

    public function test_deposit()
    {
        $snapshot = TransactionSnapshot::deposit();

        $transactionRepository = $this->mock(TransactionRepository::class);
        $transactionRepository->shouldReceive('save')->once();
        
        container()->set(TransactionRepository::class, $transactionRepository);
        
        $this->shouldPublishDomainEvents(RegisteredDeposit::class);

        dispatch(new RegisterDepositCommand(
            $snapshot['targetAssetValueId'],
            $snapshot['quantity'],
            new DateTime($snapshot['dateTime']),
        ));
    }
}