<?php namespace Tests\Transactions\Application;

use DateTime;
use Pocket\Transactions\Application\Register\RegisterExchangeCommand;
use Pocket\Transactions\Domain\Event\RegisteredExchange;
use Tests\ApplicationTest;
use Tests\Snapshots\TransactionSnapshot;
use Pocket\Transactions\Domain\TransactionRepository;

use function Quasar\Cqrs\dispatch;

class RegisterExchangeTest extends ApplicationTest {

    public function test_exchange()
    {
        $snapshot = TransactionSnapshot::Exchange();

        $transactionRepository = $this->mock(TransactionRepository::class);
        $transactionRepository->shouldReceive('save')->once();
        
        container()->set(TransactionRepository::class, $transactionRepository);
        
        $this->shouldPublishDomainEvents(RegisteredExchange::class);

        dispatch(new RegisterExchangeCommand(
            $snapshot['sourceAssetValueId'],
            $snapshot['targetAssetValueId'],
            $snapshot['quantity'],
            $snapshot['price'],
            new DateTime($snapshot['dateTime']),
        ));
    }
}