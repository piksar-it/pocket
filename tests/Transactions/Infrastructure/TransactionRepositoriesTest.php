<?php namespace Tests\Transactions\Infrastructure;

use Pocket\Transactions\Domain\Transaction;
use Pocket\Transactions\Infrastructure\TransactionRepositoryMysql;
use Tests\RepositoryTest;
use Tests\Snapshots\TransactionSnapshot;

class TransactionRepositoriesTest extends RepositoryTest {

    /**
     * @dataProvider exampleSnapshots
     */
    public function test_save_find_and_delete($exampleSnapshot)
    {
        foreach ($this->repositories() as $repository) {
            $this->abstract_save_find_and_delete_test($repository, Transaction::class, $exampleSnapshot);
        }
    }
    
    public function exampleSnapshots(): array
    {
        return [
            [TransactionSnapshot::deposit()],
            [TransactionSnapshot::withdrawal()],
            [TransactionSnapshot::exchange()],
        ];
    }

    private function repositories(): array
    {
        return [
            new TransactionRepositoryMysql(container()->get('databaseConnection')),
        ];
    }
}