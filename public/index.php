<?php

use function Quasar\Cqrs\dispatch;
use function Quasar\Ddd\uuid;

require '../bootstrap.php';

dispatch(new \Pocket\Transactions\Application\Register\RegisterTransactionCommand(uuid(), uuid(), 0.002, 1200));