<?php

use Quasar\Cqrs\CommandBus;
use Quasar\Cqrs\SimpleSyncCommandBus;
use Quasar\Ddd\Event\DomainEventPublisher;
use Quasar\Ddd\Event\EventBus;
use Quasar\Ddd\Event\SimpleEventBus;
use Quasar\Ddd\Event\SimpleSyncDomainEventPublisher;

$dbConn = static function () {
    $driver = new \Cake\Database\Driver\Mysql([
        'host'     => env('MYSQL_HOST'),
        'port'     => env('MYSQL_PORT'),
        'database' => env('MYSQL_DATABASE'),
        'username' => env('MYSQL_USER'),
        'password' => env('MYSQL_PASS'),
    ]);

    return new \Cake\Database\Connection([
        'driver'   => $driver,
        'database' => env('MYSQL_DATABASE'),
    ]);
};

return [
    'databaseConnection' => static function () use ($dbConn) { return $dbConn; },

    CommandBus::class => static function (
        \Pocket\AssetValues\Application\Add\AddNewAssetToPocketCommandHandler $addNewAssetToPocketCommandHandler,
        \Pocket\Pockets\Application\Create\CreatePocketCommandHandler $createPocketCommandHandler,
        \Pocket\Transactions\Application\Register\RegisterDepositCommandHandler $registerDepositCommandHandler,
        \Pocket\Transactions\Application\Register\RegisterWithdrawalCommandHandler $registerWithdrawalCommandHandler,
        \Pocket\Transactions\Application\Register\RegisterExchangeCommandHandler $registerExchangeCommandHandler
    ) {
        return new SimpleSyncCommandBus([
            \Pocket\AssetValues\Application\Add\AddNewAssetToPocketCommand::class => $addNewAssetToPocketCommandHandler,
            \Pocket\Pockets\Application\Create\CreatePocketCommand::class => $createPocketCommandHandler,
            \Pocket\Transactions\Application\Register\RegisterDepositCommand::class => $registerDepositCommandHandler,
            \Pocket\Transactions\Application\Register\RegisterWithdrawalCommand::class => $registerWithdrawalCommandHandler,
            \Pocket\Transactions\Application\Register\RegisterExchangeCommand::class => $registerExchangeCommandHandler
        ]);
    },

    EventBus::class => static function () {
        return new SimpleEventBus([
            \Pocket\AssetValues\Application\Update\UpdateValueAfterTransaction::class
        ]);
    },

    DomainEventPublisher::class => static function(EventBus $eventBus) {
        return new SimpleSyncDomainEventPublisher($eventBus);
    },

    # Repositories
    \Pocket\AssetValues\Domain\AssetValueRepository::class => static function () use ($dbConn) {
        return new \Pocket\AssetValues\Infrastructure\AssetValueRepositoryMysql($dbConn);
    },

    \Pocket\Pockets\Domain\PocketRepository::class => static function () use ($dbConn) {
        return new \Pocket\Pockets\Infrastructure\PocketRepositoryMysql($dbConn);
    },

    \Pocket\Transactions\Domain\TransactionRepository::class => static function () use ($dbConn) {
        return new \Pocket\Transactions\Infrastructure\TransactionRepositoryMysql($dbConn);
    },
];