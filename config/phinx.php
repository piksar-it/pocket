<?php
//usage docker-compose exec php vendor/bin/phinx migrate --configuration=/home/www/config/phinx.php

require dirname(__FILE__) . '/../bootstrap.php';

return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/../Infrastructure/Migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/../Infrastructure/Seeds'
        ],
        'environments' => [
            'default_migration_table' => 'phinx_migration_log',
            'default_database' => 'development',
            'production' => [
                'adapter' => 'mysql',
                'host' => getenv('MYSQL_HOST'),
                'name' => getenv('MYSQL_DATABASE'),
                'user' => getenv('MYSQL_USER'),
                'pass' => getenv('MYSQL_PASS'),
                'port' => getenv('MYSQL_PORT'),
                'charset' => 'utf8',
            ],
            'development' => [
                'adapter' => 'mysql',
                'host' => getenv('MYSQL_HOST'),
                'name' => getenv('MYSQL_DATABASE'),
                'user' => getenv('MYSQL_USER'),
                'pass' => getenv('MYSQL_PASS'),
                'port' => getenv('MYSQL_PORT'),
                'charset' => 'utf8',
            ],
            'testing' => [
                'adapter' => 'mysql',
                'host' => 'localhost',
                'name' => 'testing_db',
                'user' => 'root',
                'pass' => '',
                'port' => '3306',
                'charset' => 'utf8',
            ]
        ],
        'version_order' => 'creation'
    ];