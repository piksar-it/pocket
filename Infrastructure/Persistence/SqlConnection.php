<?php namespace Infrastructure\Persistence;

abstract class SqlConnection {

    protected $connectionCallback;

    private $sqlCommiter;

    /**
     * @var \Cake\Database\Connection[]
     */
    // private static $openTransactions = [];

    public function __construct(\Closure $connectionCallback)
    {
        $this->connectionCallback = $connectionCallback;
        // $this->sqlCommiter = container()->get(SqlCommiterInterface::class);
    }

    /**
     * @return \Cake\Database\Connection
     */
    protected function connection()
    {
        static $conn = null;

        if ($conn === null) {
            $conn = call_user_func($this->connectionCallback);
        }

        return $conn;
    }

    public function startTransaction(): void
    {
        $this->connection()->begin();
        // $this->addToOpenTransactions();
    }

    public function commitTransaction(): void
    {
        $this->connection()->commit();
        // $this->sqlCommiter->commitTransaction($this->connection());
        // if (!$this->sqlCommiter instanceof SqlNullCommiter) {
        //     $this->deleteFromOpenTransactions();
        // }
    }

    public function rollbackTransaction(): void
    {
        $this->connection()->rollback();
        // $this->deleteFromOpenTransactions();
    }

    private function addToOpenTransactions(): void
    {
        // $objectId = spl_object_id($this);
        // self::$openTransactions[$objectId] = $this->connection();
    }

    private function deleteFromOpenTransactions(): void
    {
        // $objectId = spl_object_id($this);
        // if (isset(self::$openTransactions[$objectId])) {
        //     unset(self::$openTransactions[$objectId]);
        // }
    }

    public static function rollbackOpenTransactions(): void
    {
        // try {
        //     foreach (self::$openTransactions as $id => $connection) {
        //         $connection->rollback();
        //         unset(self::$openTransactions[$id]);
        //     }
        // } catch (\Throwable $e) {
        //     if (!isProduction()) {
        //         d($e->getMessage());
        //     }
        // }
    }

}