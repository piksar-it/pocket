<?php namespace Infrastructure\Persistence;

interface Repository {

    public function startTransaction(): void;
    public function commitTransaction(): void;
    public function rollbackTransaction(): void;

}