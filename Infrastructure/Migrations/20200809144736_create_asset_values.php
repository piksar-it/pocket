<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateAssetValues extends AbstractMigration
{
    public function up()
    {
        $this->getTable()
            ->addColumn('uuid', 'uuid')
            ->addColumn('pocket_id', 'uuid')
            ->addColumn('symbol', 'string', ['null' => false, 'limit' => 5])
            ->addColumn('quantity', 'float')
            ->addColumn('description', 'string', ['null' => true, 'limit' => 255])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('deleted_at', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }

    public function down()
    {
        $this->getTable()->drop()->save();
    }

    private function getTable()
    {
        return $this->table('asset_values', [
            'id' => false,
            'primary_key' => ['uuid']
        ]);
    }
}
