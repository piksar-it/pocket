<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreatePockets extends AbstractMigration
{
    public function up()
    {
        $this->getPocketTable()
            ->addColumn('uuid', 'uuid')      
            ->addColumn('name', 'string', ['null' => false, 'limit' => 255])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('deleted_at', 'datetime', ['null' => true, 'default' => null])
            ->create();
        
        
        $this->getPocketAssetsTable()
            ->addColumn('uuid', 'uuid')    
            ->addColumn('pocket_id', 'uuid')   
            ->addColumn('symbol', 'string', ['null' => false, 'limit' => 5])
            ->addColumn('interest', 'float', ['null' => false,])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 255])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('deleted_at', 'datetime', ['null' => true, 'default' => null])
            ->addForeignKey('pocket_id', 'pockets', 'uuid', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'])
            ->save();
    }

    public function down()
    {
        $this->getPocketAssetsTable()->drop()->save();
        $this->getPocketTable()->drop()->save();
    }

    private function getPocketTable()
    {
        return $this->table('pockets', [
            'id' => false,
            'primary_key' => ['uuid']
        ]);
    }

    private function getPocketAssetsTable()
    {
        return $this->table('pocket_assets', [
            'id' => false,
            'primary_key' => ['uuid']
        ]);
    }
}
