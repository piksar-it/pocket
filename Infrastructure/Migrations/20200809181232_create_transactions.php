<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTransactions extends AbstractMigration
{
    public function up()
    {
        $this->getTable()
            ->addColumn('uuid', 'uuid')
            ->addColumn('datetime', 'datetime', ['null' => false,])
            ->addColumn('source_asset_value_id', 'uuid', ['null' => true,])
            ->addColumn('target_asset_value_id', 'uuid', ['null' => true,])
            ->addColumn('quantity', 'float', ['null' => false,])
            ->addColumn('price', 'float', ['null' => true,])          
            ->addColumn('description', 'string', ['null' => true, 'limit' => 255])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('deleted_at', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }

    public function down()
    {
        $this->getTable()->drop()->save();
    }

    private function getTable()
    {
        return $this->table('transactions', [
            'id' => false,
            'primary_key' => ['uuid']
        ]);
    }
}
