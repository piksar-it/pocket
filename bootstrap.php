<?php

use Dotenv\Dotenv;
use DI\ContainerBuilder;

require 'vendor/autoload.php';

$dotenv = new Dotenv(__DIR__);
$dotenv->load();


if (!empty(getenv('APP_TIMEZONE'))) {
    date_default_timezone_set(getenv('APP_TIMEZONE'));
}


if (!function_exists('container')) {
    function container(): \DI\Container
    {
        static $container;

        if ($container) {
            return $container;
        }

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAutowiring(true);
        $containerBuilder->useAnnotations(false);
        $containerBuilder->addDefinitions(__DIR__ . '/config/di.php');
        $container = $containerBuilder->build();

        return $container;
    }
}